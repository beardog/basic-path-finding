#include "MapInformation.h"

#include <cstdio>
#include <cstring>

MapInformation::MapInformation() {
  printf("%s: here\n", __func__);
  width = 0;
  height = 0;
  obstacles = nullptr;
}

MapInformation::~MapInformation() {
  printf("%s: here", __func__);
  delete obstacles;
}

int MapInformation::getHeight() const {
  return height;
}

bool MapInformation::getObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=width) || (y<0) || (y>=height)) {
    return true;
  }

  return obstacles[x + y*width];
}

int MapInformation::getWidth() const {
  return width;
}

void MapInformation::setMapSize(const int inWidth, const int inHeight) {
  width = inWidth;
  height = inHeight;

  delete obstacles;// if there was any old setup

  obstacles = new bool[width*height];
  memset(obstacles, false, width*height);
}

void MapInformation::setObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=width) || (y<0) || (y>=height)) {
    printf("%s: Bad Call (%i, %i)\n", __func__, x,y);
    return;
  }

  obstacles[x + y*width] = true;
}

