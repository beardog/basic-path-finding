#pragma once

class MapInformation {
public:
  MapInformation();
  ~MapInformation();

  void setMapSize(const int width, const int height);

  void setObstacleAt(const int x, const int y);
  bool getObstacleAt(const int x, const int y);

  int getWidth() const;
  int getHeight() const;

protected:
  bool* obstacles;
  int width;
  int height;
};
