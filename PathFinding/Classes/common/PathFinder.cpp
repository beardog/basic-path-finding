#include "PathFinder.h"
#include "MapInformation.h"

USING_NS_CC;

#include <cstring>
// #include <iostream>
// #include <iomanip>
// #include <sstream>
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

PathFinder::PathFinder(shared_ptr<MapInformation> inMapInfo,
                       const GamePos& inStart, const GamePos& inDestination) {
  start = inStart;
  destination = inDestination;
  mapInfo = inMapInfo;

  reachable.push_back(start);
}

PathFinder::~PathFinder() {
  //
}

bool PathFinder::isReachable(const int x, const int y) const {
  for(const GamePos gp : reachable) {
    // printf("%s: testing reachable %i:%i against %i:%i\n", __func__, gp.x, gp.y, x, y);
    if ((gp.x == x) && (gp.y == y)) {
      return true;
    }
  }

  // else
  return false;
}

bool PathFinder::isExplored(const int x, const int y) const {
  for(const GamePos gp : explored) {
    if ((gp.x == x) && (gp.y == y)) {
      return true;
    }
  }

  // else
  return false;
}

bool PathFinder::makeOneStep() {
  const GamePos currentNode = reachable.back();
  log("%s: current node is %i:%i\n", __func__, currentNode.x, currentNode.y);

  if ((currentNode.x == destination.x)&&(currentNode.y == destination.y)) {
    return true;
  }

  reachable.pop_back();
  explored.push_back(currentNode);

  // get new node candidates
  const int diffCount = 4;
  const int xDiff[diffCount] = {0,0,-1,1};
  const int yDiff[diffCount] = {-1,1, 0,0};

  for(int dci = 0; dci<diffCount; dci++) {
    GamePos gp;
    gp.x = currentNode.x + xDiff[dci];
    gp.y = currentNode.y + yDiff[dci];

    //if node is a wall, skip it
    if (mapInfo->getObstacleAt(gp.x, gp.y)) {
      continue;
    }

    // if node was already explored, skip it
    bool found  = false;
    for (const GamePos rgp: explored) {
      if ((rgp.x == gp.x)&&(rgp.y == gp.y)) {
        found = true;
        break;
      }
    }
    if (found) {
      continue;
    }

    //if node already in reachable, skip
    found  = false;
    for (const GamePos rgp: reachable) {
      if ((rgp.x == gp.x)&&(rgp.y == gp.y)) {
        found = true;
        break;
      }
    }

    //finally add to reachable
    if (!found) {
      reachable.push_back(gp);
    }
  }

  return false;
}