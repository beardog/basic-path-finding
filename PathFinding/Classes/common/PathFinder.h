#pragma once

#include "cocos2d.h"
#include <memory>
#include <list>

#include "GamePos.h"

class MapInformation;

class PathFinder {
public:
  PathFinder(std::shared_ptr<MapInformation> mapInfo,
             const GamePos& start, const GamePos& destination);
  virtual ~PathFinder();

  bool isReachable(const int x, const int y) const;
  bool isExplored(const int x, const int y) const;

  // returns true if this is last move
  bool makeOneStep();

protected:

  GamePos start;
  GamePos destination;

  std::list<GamePos> reachable;
  std::list<GamePos> explored;

  std::shared_ptr<MapInformation> mapInfo;
};
