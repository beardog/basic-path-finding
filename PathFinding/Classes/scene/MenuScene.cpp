#include "MenuScene.h"
#include "SmallerDemoScene.h"

USING_NS_CC;
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

enum SceneCode {
  SC_FirstAvailable = 10,
  SC_CandleMonster,
  SC_Skeleton
};

enum z_orders {
  ZO_BACKGROUND = 0,
  ZO_MENU = 10
};

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Scene * MenuScene::createScene() {
  return MenuScene::create();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

typedef bool (MenuScene::*init_method)();

bool MenuScene::init() {
  //////////////////////////////
  // 1. super init first
  if (!Scene::init()) {
    return false;
  }

  const int initStepsCount = 3;
  init_method initSteps[initStepsCount] = {
    &MenuScene::initBackground, &MenuScene::initMenuButtons, &MenuScene::initKeyboardProcessing
  };

  for(int i = 0; i<initStepsCount; i++) {
    init_method initStep = initSteps[i];
    bool boo = (this->*initStep)();
    if (!boo) {
      log("%s: Step #%i failed", __func__, i);
      return false;
    }
  }

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MenuScene::initBackground() {
  const char backFilename[] = "background/background.png";

  Sprite* sprite = Sprite::create(backFilename);
  if (sprite == nullptr) {
    printf("Error while loading: %s\n", backFilename);
    return false;
  }

  sprite->setAnchorPoint(Vec2(0,0));
  sprite->setPosition(0,0);
  addChild(sprite, ZO_BACKGROUND);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MenuScene::initKeyboardProcessing() {
  EventListenerKeyboard* sceneKeyboardListener = EventListenerKeyboard::create();
  sceneKeyboardListener->onKeyPressed = CC_CALLBACK_2(MenuScene::onKeyPressedScene, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(sceneKeyboardListener, this);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MenuScene::initMenuButtons() {
  SpriteFrameCache* sfc = SpriteFrameCache::getInstance();

  const string uiFilename = "ui/ui_buttons.plist";

  sfc->addSpriteFramesWithFile(uiFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(uiFilename)) {
    printf("Error while loading: %s\n", uiFilename.c_str());
    return false;
  }

  Menu* menu = Menu::create();
  menu->setPosition(320,180);

  const int itemsCount = 3;
  string captions[itemsCount] = {"First come-at-able", "Shortest Explored", "A*"};
  int callbackCodes[itemsCount] = {SC_FirstAvailable, SC_CandleMonster, SC_Skeleton};

  for (int i = 0; i< itemsCount; i++) {
    MenuItemImage* item = MenuItemImage::create();
    item->setNormalSpriteFrame(sfc->getSpriteFrameByName("blue_button_long.png"));
    item->setSelectedSpriteFrame(sfc->getSpriteFrameByName("green_button_long.png"));
    item->setCallback(CC_CALLBACK_1(MenuScene::switchToNewScene, this,callbackCodes[i]));

    Label* label = Label::createWithTTF(captions[i], "fonts/Marker Felt.ttf", 18);
    label->setAnchorPoint(Vec2(0.5,0.5));
    const Size itemSize = item->getContentSize();
    label->setPosition(itemSize.width/2, itemSize.height/2);
    item->addChild(label);

    menu->addChild(item);
  }

  addChild(menu, ZO_MENU);

  menu->alignItemsVertically();

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MenuScene::switchToNewScene(cocos2d::Ref *pSender, const int sceneCode) {
  Scene* newScene = nullptr;

  switch (sceneCode) {
  case SC_FirstAvailable:
    newScene = SmallerDemoScene::create();
    break;
  case SC_CandleMonster:
    // newScene = CandleMonsterScene::create();
    break;
  case SC_Skeleton:
    // newScene = SkeletonScene::create();
    break;
  default:
    log("%s: bad scene code value", __func__);
  }

  if (newScene) {
    Director::getInstance()->pushScene(newScene);
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MenuScene::onKeyPressedScene(EventKeyboard::KeyCode keyCode, Event *) {
  if (EventKeyboard::KeyCode::KEY_X == keyCode) {
    log("%s:Need to get out.", __func__);

    // Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .