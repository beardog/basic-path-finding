#include "SmallerDemoScene.h"
#include "common/MapInformation.h"
#include "common/PathFinder.h"

USING_NS_CC;

#include <regex>
#include <cstring>
// #include <iostream>
// #include <iomanip>
// #include <sstream>
using namespace std;

enum ZOrderConstants {
  ZO_BACKGROUND,
  ZO_WALLS,
  ZO_PATH_MARKERS,
  ZO_LIST_MARKERS,
  ZO_ITEM
};


struct MarkerSpriteNames {
  string obstacle;
  string reachable;
  string explored;
};

static const MarkerSpriteNames markerSpriteNames = {
  .obstacle = "cross.png",
  .reachable = "green_circle.png",
  .explored = "red_circle.png"
};

static const string nodeNameWall = "wall";
static const string nodeNameStart = "start";
static const string nodeNameDestination = "destination";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


// PathFinder::PathFinder(shared_ptr<MapInformation> inMapInfo,
//                        const GamePos& inStart, const GamePos& inDestination) {
//   start = inStart;
//   destination = inDestination;
//   mapInfo = inMapInfo;

//   reachable.push_back(start);
// }

// PathFinder::~PathFinder() {
//   //
// }

// bool PathFinder::isReachable(const int x, const int y) const {
//   for(const GamePos gp : reachable) {
//     // printf("%s: testing reachable %i:%i against %i:%i\n", __func__, gp.x, gp.y, x, y);
//     if ((gp.x == x) && (gp.y == y)) {
//       return true;
//     }
//   }

//   // else
//   return false;
// }

// bool PathFinder::isExplored(const int x, const int y) const {
//   for(const GamePos gp : explored) {
//     if ((gp.x == x) && (gp.y == y)) {
//       return true;
//     }
//   }

//   // else
//   return false;
// }

// bool PathFinder::makeOneStep() {
//   const GamePos currentNode = reachable.back();
//   log("%s: current node is %i:%i\n", __func__, currentNode.x, currentNode.y);

//   if ((currentNode.x == destination.x)&&(currentNode.y == destination.y)) {
//     return true;
//   }

//   reachable.pop_back();
//   explored.push_back(currentNode);

//   // get new node candidates
//   const int diffCount = 4;
//   const int xDiff[diffCount] = {0,0,-1,1};
//   const int yDiff[diffCount] = {-1,1, 0,0};

//   for(int dci = 0; dci<diffCount; dci++) {
//     GamePos gp;
//     gp.x = currentNode.x + xDiff[dci];
//     gp.y = currentNode.y + yDiff[dci];

//     //if node is a wall, skip it
//     if (mapInfo->getObstacleAt(gp.x, gp.y)) {
//       continue;
//     }

//     // if node was already explored, skip it
//     bool found  = false;
//     for (const GamePos rgp: explored) {
//       if ((rgp.x == gp.x)&&(rgp.y == gp.y)) {
//         found = true;
//         break;
//       }
//     }
//     if (found) {
//       continue;
//     }

//     //if node already in reachable, skip
//     found  = false;
//     for (const GamePos rgp: reachable) {
//       if ((rgp.x == gp.x)&&(rgp.y == gp.y)) {
//         found = true;
//         break;
//       }
//     }

//     //finally add to reachable
//     if (!found) {
//       reachable.push_back(gp);
//     }
//   }

//   return false;
// }

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SmallerDemoScene::SmallerDemoScene() {
  log("%s: here\n", __func__);
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

SmallerDemoScene::~SmallerDemoScene() {
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Scene * SmallerDemoScene::createScene() {
  return SmallerDemoScene::create();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 SmallerDemoScene::gamePosToReal(const int x, const int y) const {
  const int tileSize = 32;
  const int tileSizeHalf = tileSize/2;

  const int rx = x*tileSize + tileSizeHalf;
  const int ry = y*tileSize + tileSizeHalf;

  return Vec2(rx, ry);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Vec2 SmallerDemoScene::gamePosToReal(const GamePos& gamePos) const {
  const int tileSize = 32;
  const int tileSizeHalf = tileSize/2;

  const int rx = gamePos.x*tileSize + tileSizeHalf;
  const int ry = gamePos.y*tileSize + tileSizeHalf;

  return Vec2(rx, ry);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// on "init" you need to initialize your instance
bool SmallerDemoScene::init() {
  log("%s: here\n", __func__);
  //////////////////////////////
  // 1. super init first
  if (!Scene::init()) {
    return false;
  }

  log("%s: a", __func__);

  if (!initBackground()) {
    return false;
  }

  log("%s: b", __func__);
  if (!initMarkers()) {
    return false;
  }
  log("%s: c", __func__);
  if (!initMap()) {
    return false;
  }
  log("%s: d", __func__);

  if (!initButtons()) {
    return false;
  }
  log("%s: f", __func__);

  // keyboard processing
  EventListenerKeyboard* sceneKeyboardListener = EventListenerKeyboard::create();
  sceneKeyboardListener->onKeyPressed = CC_CALLBACK_2(SmallerDemoScene::onKeyPressedScene, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(sceneKeyboardListener, this);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SmallerDemoScene::initBackground() {
  const char backFilename[] = "background_2.png";

  Sprite* sprite = Sprite::create(backFilename);
  if (sprite == nullptr) {
    printf("Error while loading: %s\n", backFilename);
    return false;
  }

  sprite->setAnchorPoint(Vec2(0,0));
  sprite->setPosition(0,0);
  addChild(sprite, ZO_BACKGROUND);

  const char gameNodeBack[] = "background_g1.png";

  gameNode = Sprite::create(gameNodeBack);
  if (gameNode == nullptr) {
    printf("Error while loading: %s\n", gameNodeBack);
    return false;
  }

  gameNode->setAnchorPoint(Vec2(0.5,0.5));
  Size bsSize = sprite->getContentSize();
  gameNode->setPosition(bsSize.width/2,bsSize.height/2);
  sprite->addChild(gameNode, ZO_BACKGROUND);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SmallerDemoScene::initMap() {
  mapInfo = make_shared<MapInformation>();

  resetToCaseA(nullptr);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SmallerDemoScene::initMarkers() {
  SpriteFrameCache* const sfc = SpriteFrameCache::getInstance();

  const string plistFilename = "markers/markers.plist";
  sfc->addSpriteFramesWithFile(plistFilename);
  if (!sfc->isSpriteFramesWithFileLoaded(plistFilename)) {
    log("%s: failed to load %s file", __func__, plistFilename.c_str());
    return false;
  }

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool SmallerDemoScene::initButtons() {
  MenuItemImage* makeStepButton = MenuItemImage::create(
    "ui/rightArrow1.png", "ui/rightArrow2.png",
    CC_CALLBACK_1(SmallerDemoScene::oneStepCallback, this));
  if(makeStepButton == nullptr) {
    printf("%s: failed to load button\n", __func__);
    return false;
  }

  makeStepButton->setAnchorPoint(Vec2(1,0.5));
  makeStepButton->setPosition(Vec2(640, 120));

  Menu* menu = Menu::create(makeStepButton, nullptr);
  menu->setPosition(Vec2::ZERO);


  const int buttonsCount = 2;
  // MenuItemImage* buttons[buttonsCount];
  string captions[buttonsCount] = {"Case A", "Case B"};
  ccMenuCallback callbacks[buttonsCount] = {
    CC_CALLBACK_1(SmallerDemoScene::resetToCaseA, this),
    CC_CALLBACK_1(SmallerDemoScene::resetToCaseB, this)
  };

  for (int i = 0; i<buttonsCount; i++ ) {
    MenuItemImage* button = MenuItemImage::create("ui/blue_button.png", "ui/blue_button.png",
                                                  callbacks[i]);
    if(button == nullptr) {
      printf("%s: failed to load button\n", __func__);
      return false;
    }

    const Size blueButtonSize = button->getContentSize();

    Label* label = Label::createWithTTF(captions[i], "fonts/Marker Felt.ttf", 16);
    if(label == nullptr) {
      printf("%s: failed to create label\n", __func__);
      return false;
    }
    label->setAnchorPoint(Vec2(0.5,0.5));
    label->setPosition(blueButtonSize.width/2, blueButtonSize.height/2);
    button->addChild(label);
    button->setAnchorPoint(Vec2(1,0.5));
    button->setPosition(Vec2(640,
                             360- blueButtonSize.height/2 - 10*(i+1)- (blueButtonSize.height)*i));

    menu->addChild(button);
  }

  addChild(menu);

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::oneStepCallback(Ref* pSender) {
  if(pathFinder == nullptr) {
    pathFinder = make_shared<PathFinder>(mapInfo, start, destination);
  }
  else {
    pathFinder->makeOneStep();
  }

  for(Node* node : pathMarkers) {
    node->removeFromParent();
  }
  pathMarkers.clear();


  const int width = mapInfo->getWidth();
  const int height = mapInfo->getHeight();

  for (int i = 0; i<width; i++) {
    for (int j = 0; j< height; j++) {
      Sprite* markerSprite = nullptr;
      if (pathFinder->isReachable(i,j)) {
        // printf("%s: pos %i:%i is reachable\n", __func__, i,j);
        markerSprite = Sprite::createWithSpriteFrameName(markerSpriteNames.reachable);
      }
      else if (pathFinder->isExplored(i,j)) {
        markerSprite = Sprite::createWithSpriteFrameName(markerSpriteNames.explored);
      }

      if (markerSprite) {
        markerSprite->setPosition(gamePosToReal(i,j));
        gameNode->addChild(markerSprite, ZO_WALLS);
        pathMarkers.push_back(markerSprite);
      }

    }
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::onKeyPressedScene(EventKeyboard::KeyCode keyCode,
                                         Event                 *event) {
  printf("%s: processing key %d pressed\n", __func__, (int)keyCode);

  if (EventKeyboard::KeyCode::KEY_SPACE == keyCode) {
    printf("%s: Space was pressed (but nothing to do)\n", __func__);
  }
  else if (EventKeyboard::KeyCode::KEY_X == keyCode) {
    printf("%s: Need to get out.\n", __func__);

    // Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::placeEndsMarkers() {
  Sprite* markerSprite;
  markerSprite = Sprite::createWithSpriteFrameName("m_1.png");
  if (markerSprite == nullptr) {
    return;
  }
  markerSprite->setPosition(gamePosToReal(start));
  gameNode->removeChildByName(nodeNameStart);
  gameNode->addChild(markerSprite, ZO_PATH_MARKERS, nodeNameStart);

  markerSprite = Sprite::createWithSpriteFrameName("m_2.png");
  if (markerSprite == nullptr) {
    return;
  }
  markerSprite->setPosition(gamePosToReal(destination));
  gameNode->removeChildByName(nodeNameDestination);
  gameNode->addChild(markerSprite, ZO_PATH_MARKERS, nodeNameDestination);

}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::recreateWallMarkers() {
  for(Node* node : wallMarkers) {
    node->removeFromParent();
  }
  wallMarkers.clear();

  const int width = mapInfo->getWidth();
  const int height = mapInfo->getHeight();

  // int nnGen = 0;
  // ostringstream ss;

  for (int i = 0; i<width; i++) {
    for (int j = 0; j< height; j++) {
      if (!mapInfo->getObstacleAt(i,j)) {
        continue;
      }

      Sprite* markerSprite = Sprite::createWithSpriteFrameName(markerSpriteNames.obstacle);
      markerSprite->setPosition(gamePosToReal(i,j));

      gameNode->addChild(markerSprite, ZO_WALLS);
      wallMarkers.push_back(markerSprite);
      // ss << nodeNameWall << nnGen++;
      // printf("%s: will add node with name'%s'\n", __func__,ss.str().c_str());
      // gameNode->addChild(markerSprite, ZO_WALLS, ss.str());
      // ss.str("");
    }
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::resetToCaseA(Ref*) {
  for(Node* node : pathMarkers) {
    node->removeFromParent();
  }
  pathMarkers.clear();

  start.x = 0;
  start.y = 4;
  destination.x = 4;
  destination.y = 1;
  placeEndsMarkers();


  mapInfo->setMapSize(5,5);
  mapInfo->setObstacleAt(0,0);

  mapInfo->setObstacleAt(0,1);
  mapInfo->setObstacleAt(2,1);
  mapInfo->setObstacleAt(3,1);

  mapInfo->setObstacleAt(1,3);
  mapInfo->setObstacleAt(2,3);
  mapInfo->setObstacleAt(3,3);

  mapInfo->setObstacleAt(3,4);

  recreateWallMarkers();

  pathFinder = nullptr;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void SmallerDemoScene::resetToCaseB(Ref*) {
  // printf("%s: here\n", __func__);
  for(Node* node : pathMarkers) {
    node->removeFromParent();
  }
  pathMarkers.clear();

  start.x = 2;
  start.y = 4;
  destination.x = 2;
  destination.y = 0;
  placeEndsMarkers();

  mapInfo->setMapSize(5,5);

  mapInfo->setObstacleAt(0,2);
  mapInfo->setObstacleAt(1,2);
  mapInfo->setObstacleAt(2,2);

  recreateWallMarkers();

  pathFinder = nullptr;

  // gameNode->removeChildByName(nodeNameWall);

  // consr Vector<Node*> allChildren = no

  // ostringstream ss;
  // ss<< nodeNameWall << "[[:digit:]]+";

  // const regex nodeNameRegex(ss.str());

  // for (Node* node: gameNode->getChildren()) {
  //   if (regex_match(node->getName(), nodeNameRegex)) {
  //     printf("matched one node (%s)\n", node->getName().c_str());
  //     node->removeFromParent();
  //   }
  //   else {
  //     printf("does not match with (%s)\n", node->getName().c_str());
  //   }
  // }



  // gameNode->enumerateChildren(ss.str(), [](Node* node) -> bool {
  //   printf("removed one node (%s)\n", node->getName().c_str());
  //   node->removeFromParent();
  //   return false;
  // });




}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
