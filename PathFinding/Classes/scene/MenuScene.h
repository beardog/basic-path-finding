#pragma once

#include "cocos2d.h"

class MenuScene : public cocos2d::Scene {
public:

  static cocos2d::Scene* createScene();

  virtual bool           init();


protected:
  bool initBackground();
  bool initMenuButtons();
  bool initKeyboardProcessing();

  void switchToNewScene(cocos2d::Ref *pSender, const int sceneCode);

  void onKeyPressedScene(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *);


// implement the "static create()" method manually
  CREATE_FUNC(MenuScene);
};
