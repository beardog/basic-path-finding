#pragma once

#include "cocos2d.h"
#include <memory>
#include <list>

#include "common/GamePos.h"

class MapInformation;
class PathFinder;

// class PathFinder {
// public:
//   PathFinder(std::shared_ptr<MapInformation> mapInfo,
//              const GamePos& start, const GamePos& destination);
//   virtual ~PathFinder();

//   bool isReachable(const int x, const int y) const;
//   bool isExplored(const int x, const int y) const;

//   // returns true if this is last move
//   bool makeOneStep();

// protected:

//   GamePos start;
//   GamePos destination;

//   std::list<GamePos> reachable;
//   std::list<GamePos> explored;

//   std::shared_ptr<MapInformation> mapInfo;
// };



class SmallerDemoScene : public cocos2d::Scene {
public:

  static cocos2d::Scene* createScene();

  virtual bool           init();

// implement the "static create()" method manually
  CREATE_FUNC(SmallerDemoScene);

protected:
  SmallerDemoScene();
  virtual ~SmallerDemoScene();

  void onKeyPressedScene(cocos2d::EventKeyboard::KeyCode keyCode,
                         cocos2d::Event                 *event);

  bool initBackground();
  bool initButtons();
  bool initMap();
  bool initMarkers();
  bool initPathMarkers();

  std::shared_ptr<PathFinder> pathFinder;

  cocos2d::Vec2 gamePosToReal(const int x, const int y) const;
  cocos2d::Vec2 gamePosToReal(const GamePos& gamePos) const;

  void oneStepCallback(cocos2d::Ref *pSender);
  void resetToCaseA(cocos2d::Ref *pSender);
  void resetToCaseB(cocos2d::Ref *pSender);

  void recreateWallMarkers();
  void placeEndsMarkers();

  cocos2d::Node* gameNode;
  std::list<cocos2d::Node*> wallMarkers;
  std::list<cocos2d::Node*> pathMarkers;

  GamePos start;
  GamePos destination;

  std::shared_ptr<MapInformation> mapInfo;
};
